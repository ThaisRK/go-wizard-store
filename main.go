package main

import (
	"html/template"
	"net/http"
)

type Produto struct {
	Nome       string
	Descricao  string
	Preco      float64
	Quantidade int
}

var temp = template.Must(template.ParseGlob("templates/*.html"))

func main() {
	http.HandleFunc("/", index)
	http.ListenAndServe(":8000", nil)
}

func index(w http.ResponseWriter, r *http.Request) {
	produtos := []Produto{
		{"Varinha", "Fibra de coração de dragão", 89, 3},
		{"Poção", "Polissuco", 59, 2},
		{"Livro", "Proibido", 1.99, 1},
		{"Frasco", "Super resistente", 21, 1},
	}

	temp.ExecuteTemplate(w, "Index", produtos)
}
