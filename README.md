<a href="https://gitmoji.dev">
  <img
    src="https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=flat-square"
    alt="Gitmoji"
  />
</a>

# GO Wizard Store

This is a studying web project in order to practice and enhance my technical skills as a GO developer.

It first started as a project from Alura's course "Go: crie uma aplicação web". The ideia henceforth is to keep improving it.

## Getting started

In order to run the application, run `go run main.go`
